﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;

public partial class Orders : System.Web.UI.Page
{
    String prodID = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        prodID = Request.QueryString["prod"];
        OleDbConnection myConn = new OleDbConnection();
        OleDbDataReader myReader;
        myConn.ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["AcmeShoppeConnectionString"].ConnectionString;
        myConn.Open();
        String sqlQuery = String.Format("SELECT Name, ShortDescription, LongDescription, ImageFile, UnitPrice FROM Products WHERE ProductID = '{0}';", prodID);
        OleDbCommand sqlCmd = new OleDbCommand(sqlQuery, myConn);
        myReader = sqlCmd.ExecuteReader();
        while (myReader.Read())
        {
            String productName = myReader["Name"].ToString();
            String shortDesc = myReader["ShortDescription"].ToString();
            String longDesc = myReader["LongDescription"].ToString();
            String imageFile = myReader["ImageFile"].ToString();
            decimal unitPrice = Decimal.Parse(myReader["UnitPrice"].ToString());
            this.lblProductName.Text = productName;
            this.lblShortDesc.Text = shortDesc;
            this.lblLongDesc.Text = longDesc;
            this.imgProduct.ImageUrl = "Images/Products/" + imageFile;
            this.lblPrice.Text =  unitPrice.ToString();
        }
        myReader.Close();
        myConn.Close();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        String productName = this.lblProductName.Text;
        prodID = Request.QueryString["prod"];
        decimal unitPrice = Decimal.Parse(this.lblPrice.Text);
        decimal decQty = Convert.ToDecimal(this.txtQty.Text);
        decimal decTotal = decQty * unitPrice;
        string sqlString;
        if (Session["cartnumber"] == null) 
        {
            Int32 intcartnumber = getCounterValue("CartNumber");
            Session["cartnumber"] = intcartnumber;
        }
        OleDbConnection myConn = new OleDbConnection();
        myConn.ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["AcmeShoppeConnectionString"].ConnectionString;
        if (myConn.State == ConnectionState.Closed) myConn.Open();
        sqlString = "insert into cart_lineitems values (" + Session["cartnumber"] +
        ", '" + prodID + "', "  + decQty.ToString() + ");";
        OleDbCommand myCommand = new OleDbCommand(sqlString, myConn);
        try
        {
            myCommand.ExecuteNonQuery();
            myConn.Close();
            Response.Redirect("Cart.aspx");
        }
        catch (Exception ex)
        {
            this.lblProductName.Text = "Cannot add record" + ex.Message + sqlString;
        }
        myConn.Close();
    }
    private int getCounterValue(string strKey)
    {
        int count;
        string query = "SELECT idnumber FROM controltable " +
        "WHERE ctlkey = '" + strKey + "';";
        OleDbConnection myConn = new OleDbConnection();
        OleDbDataReader myReader;
        myConn.ConnectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["AcmeShoppeConnectionString"].ConnectionString;
        OleDbCommand myCmd = new OleDbCommand(query, myConn);
        myConn.Open();
        myReader = myCmd.ExecuteReader();
        myReader.Read();
        count = Convert.ToInt32(myReader[0].ToString()) + 1; ;
        myReader.Close();
        string updateQuery = "UPDATE controltable SET idnumber = " + count +
        " where ctlkey = '" + strKey + "';";
        myCmd = new OleDbCommand(updateQuery, myConn);
        myCmd.ExecuteNonQuery();
        myConn.Close();
        return count;
    }
    protected void cart_btn_Click(object sender, EventArgs e)
    {

        Response.Redirect("Cart.aspx");

    }
}