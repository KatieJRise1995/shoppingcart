﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;

public partial class Order : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        {
            if (!this.IsPostBack) this.DropDownList1.DataBind();
            this.getSelectedProduct();
        }
    }
    private void getSelectedProduct()
    {
        string sqlString = "SELECT Name, ShortDescription, LongDescription, ImageFile," +
        "UnitPrice FROM Products " +
        "WHERE ProductID = '" + this.DropDownList1.SelectedValue + "';";
        OleDbConnection myConn = new OleDbConnection();
        OleDbDataReader myReader;
        myConn.ConnectionString = this.SqlDataSource1.ConnectionString;
        OleDbCommand myCmd = new OleDbCommand(sqlString, myConn);
        myConn.Open();
        myReader = myCmd.ExecuteReader();
        myReader.Read();
        this.lblName.Text = myReader[0].ToString();
       // this.lblLongDescription.Text = myReader[2].ToString();
      //  this.lblShortDescription.Text = myReader[1].ToString();
        this.lblUnitPrice.Text = myReader[4].ToString();
        this.Image2.ImageUrl = "images/products/" + myReader[3].ToString();
        myReader.Close();
        myConn.Close();
    }
    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string strProduct = this.DropDownList1.SelectedValue;
        decimal decQty = Convert.ToDecimal(this.TextBox1.Text);
        string strName = this.lblName.Text;
        decimal decPrice = Convert.ToDecimal(this.lblUnitPrice.Text);
        decimal decTotal = decQty * decPrice;
        string sqlString;
        OleDbConnection myConn = new OleDbConnection();
        myConn.ConnectionString = this.SqlDataSource1.ConnectionString;
        if (myConn.State == ConnectionState.Closed) myConn.Open();
        sqlString = "insert into MyCart values (" + Session["cartnumber"] +
        ", '" + strProduct + "', '" + strName + "', " +
        decQty.ToString() + ", " + decPrice.ToString() + "," + decTotal.ToString() + ");";
        OleDbCommand myCommand = new OleDbCommand(sqlString, myConn);
        try
        {
            myCommand.ExecuteNonQuery();
            myConn.Close();
            Response.Redirect("Cart.aspx");
        }
        catch (Exception ex)
        {
            this.lblName.Text = "Cannot add record" + ex.Message + sqlString;
        }
        myConn.Close();
    }
    protected void cart_btn_Click(object sender, EventArgs e)
    {
      
            Response.Redirect("Cart.aspx");
   
    }
}