﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Cart.aspx.cs" Inherits="Cart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:AcmeShoppeConnectionString %>" 
        DeleteCommand="DELETE FROM [Cart_LineItems] WHERE [CartNumber] = ? AND [ProductID] = ?"  
        ProviderName="<%$ ConnectionStrings:AcmeShoppeConnectionString.ProviderName %>" 
        SelectCommand="SELECT * FROM [Cart_View02] WHERE ([CartNumber] = ?)" 
        UpdateCommand="UPDATE [Cart_LineItems] SET [Quantity] = ? WHERE [CartNumber] = ? AND [ProductID] = ?">
        <DeleteParameters>
            <asp:Parameter Name="CartNumber" Type="Int32" />
            <asp:Parameter Name="ProductID" Type="String" />
        </DeleteParameters>
        <SelectParameters>
            <asp:SessionParameter DefaultValue="100" Name="CartNumber" SessionField="cartnumber" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Quantity" Type="Int32" />
            <asp:Parameter Name="CartNumber" Type="Int32" />
            <asp:Parameter Name="ProductID" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSource1" DataKeyNames="cartnumber,productid" ForeColor="#333333" GridLines="None" Width="600px">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True">
            <ItemStyle Width="110px" />
            </asp:CommandField>
            <asp:TemplateField HeaderText="ImageFile" SortExpression="ImageFile">
                <EditItemTemplate>
                    <asp:image ImageUrl='<%#Eval("ImageFile", "images/products/{0}") %>' width="90px" Height="100px" ID="Label1" runat="server" Text='<%# Eval("ImageFile") %>'></asp:image>
                </EditItemTemplate>
                <ItemTemplate>
                     <asp:image ImageUrl='<%#Eval("ImageFile", "images/products/{0}") %>' width="90px" Height="100px" ID="Label1" runat="server" Text='<%# Eval("ImageFile") %>'></asp:image>
                </ItemTemplate>
                <HeaderStyle Height="100px" Width="90px" />
                <ItemStyle Width="90px" VerticalAlign="Top" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Name" SortExpression="Name">
                <EditItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                    
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:HyperLink ID="link1" NavigateUrl='<%#Eval("productid","order.aspx?prod={0}") %>' runat="server" Text='<%#Eval("Name")%>'></asp:HyperLink>
                    <br />
                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("Productid") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="175px" VerticalAlign="Top" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Price" SortExpression="UnitPrice">
                <EditItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("UnitPrice") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("UnitPrice","{0:c}") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Right" />
                <ItemStyle Width="75px" HorizontalAlign="Right" VerticalAlign="Top" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Quantity" SortExpression="Quantity">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Quantity") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvQty" runat="server" ErrorMessage="*" ControlToValidate="TextBox1"></asp:RequiredFieldValidator>
                </EditItemTemplate>
                <FooterTemplate>
                     Total
                </FooterTemplate> 
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("Quantity") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Right" />
                <ItemStyle Width="75px" HorizontalAlign="Right" VerticalAlign="Top" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Total" SortExpression="extension">
                <EditItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("extension") %>'></asp:Label>
                </EditItemTemplate>
                <FooterTemplate>
                     Total
                </FooterTemplate> 
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("extension","{0:c}") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Right" />
                <ItemStyle Width="75px" HorizontalAlign="Right" VerticalAlign="Top" />
            </asp:TemplateField>
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False" />
</asp:Content>

