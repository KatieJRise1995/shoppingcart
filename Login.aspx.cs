﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.IsPostBack == false)
            this.newuser_panel.Visible = false;
    }
    protected void cmdSubmit_Click(object sender, EventArgs e)
    {
        string strUserid, strPassword;
        strUserid  = txtUserID.Text;
        strPassword = txtPassword.Text;
        if (strUserid == "Admin") 
            System.Web.Security.FormsAuthentication.RedirectFromLoginPage(strUserid, false);
        else
        {
            int customerId = getCustomerRecord(strUserid,strPassword);
            if (customerId == 0) this.lblError1.Text = "User/PWD does not exist";
            else
            {
                Session["custid"] = customerId.ToString();
                System.Web.Security.FormsAuthentication.RedirectFromLoginPage(strUserid, false);
            }
        }
    }

    protected void cmdAddUser_Click(object sender, EventArgs e)
    {
        string error = "";
        OleDbConnection myConn = new OleDbConnection();
        myConn.ConnectionString = SqlDataSource1.ConnectionString;
        string sql;
        int customerId = 136;
        string struserid = "wirthw@matc.edu";

        sql = "INSERT into customers (Email, LastName, FirstName, Address, City, State, ZipCode, PhoneNumber, Pwd) " +
        "add your statements here";
        myConn.Open();
        OleDbCommand myCmd = new OleDbCommand(sql, myConn);
        try
         {
            myCmd.ExecuteNonQuery();
            myConn.Close();
            lblError2.Text = "Customer Added";
            //struserid = txtUserID2.Text;
            //customerId = getCustomerRecord(txtUserID2.Text, TxtPassword2.Text);
            Session["custid"] = customerId;
           System.Web.Security.FormsAuthentication.RedirectFromLoginPage(struserid, false);
         }
        catch (Exception ex)
         {
            lblError2.Text = "An error has occured: " + ex.Message;
            myConn.Close();
         }
    }

    private int getCustomerRecord(string userId, string pwd)
    {
        string sql, email;
        int customerId = 0;
        string password = "";

        sql = "SELECT custnumber, Email, pwd FROM customers " +
              "WHERE Email = '" + userId + "'";

        OleDbConnection myConn = new OleDbConnection();
        OleDbDataReader myReader;
        myConn.ConnectionString = SqlDataSource1.ConnectionString;
        OleDbCommand myCmd = new OleDbCommand(sql, myConn);
        myConn.Open();
        myReader = myCmd.ExecuteReader();
        while (myReader.Read())
        {
            customerId = Convert.ToInt32(myReader["custnumber"].ToString());
            email = myReader["email"].ToString();
            password = myReader["pwd"].ToString();
        }
        myReader.Close();
        if (password.Length == 0)
            customerId = 0;
        else if (password.ToUpper() != pwd.ToUpper())
            customerId = 0;
        myConn.Close();
        return customerId;
    }

    protected void btnNewUser_Click(object sender, EventArgs e)
    {
        this.logon_panel.Visible = false;
        this.newuser_panel.Visible = true;
    }
    protected void btnExistingUser_Click(object sender, EventArgs e)
    {
        this.newuser_panel.Visible = false;
        this.logon_panel.Visible = true;
    }
}