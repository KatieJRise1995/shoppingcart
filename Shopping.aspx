﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Shopping.aspx.cs" Inherits="Shopping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:AcmeShoppeConnectionString %>" ProviderName="<%$ ConnectionStrings:AcmeShoppeConnectionString.ProviderName %>" SelectCommand="SELECT [ProductID], [Name], [ImageFile] FROM [Products] WHERE ([CategoryID] = ?)">
        <SelectParameters>
            <asp:QueryStringParameter DefaultValue="fx" Name="CategoryID" QueryStringField="cat" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:DataList ID="DataList1" runat="server" DataKeyField="ProductID" DataSourceID="SqlDataSource1" RepeatColumns="3">
        <ItemTemplate>

            <div style="width: 215px; padding:0 5 25px 0; text-align:center;">
                <asp:HyperLink ID="HyperLink1" runat="server"
                     Text='<%# Eval("Name") %>' 
                    NavigateUrl='<%# "orders.aspx?prod=" + Eval("ProductID") %>'>HyperLink</asp:HyperLink>
                <asp:Image ID="Image1" runat="server" 
                    ImageUrl='<%# Eval("ImageFile", "images/products/{0}") %>' Height="250px" width="190px"
                    />   

                </div>
        </ItemTemplate>
    </asp:DataList>
</asp:Content>

