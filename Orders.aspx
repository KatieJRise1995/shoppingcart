﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Orders.aspx.cs" Inherits="Orders" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Label ID="lblProductName" runat="server"></asp:Label>
    <br />
    <br />
    <asp:Image ID="imgProduct" runat="server" ImageAlign="Right" />
    <br />
    <asp:Label ID="lblShortDesc" runat="server"></asp:Label>
    <br />
    <br />
    <asp:Label ID="lblLongDesc" runat="server"></asp:Label>
    <br />
    <br />
    Price: <asp:Label ID="lblPrice" runat="server"></asp:Label>
    <asp:TextBox ID="txtQty" runat="server" Text="1"></asp:TextBox>
    <br />
    <br />
    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Add to Cart" />
    <br />
    <br />
</asp:Content>

