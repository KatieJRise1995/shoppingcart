﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style2 {
            width: 100%;
        }
        .auto-style4 {
            width: 78px;
            height: 23px;
        }
        .auto-style5 {
            height: 23px;
        }
        .auto-style6 {
            width: 78px;
        }
        .auto-style7 {
            width: 78px;
            height: 26px;
        }
        .auto-style8 {
            height: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>

          <asp:Panel ID="logon_panel" runat="server" Height="250px" Width="409px">
               <table style="width: 390px">
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="UserID:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtUserID" runat="server" Columns="30" Width="283px" ValidationGroup="newcust">wirthw@matc.edu</asp:TextBox>
 
                    <asp:RequiredFieldValidator ID="RFV1" runat="server" ControlToValidate="txtUserID" Display="Dynamic" 
                        ErrorMessage="Email Adress is required" ForeColor="Red" text="*" 
                        ValidationGroup="oldcust"></asp:RequiredFieldValidator>
 
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Password:"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtPassword" runat="server" Columns="15" 
                        MaxLength="15" ValidationGroup="newcust">qwerty</asp:TextBox>
 
                    <asp:RequiredFieldValidator ID="RFV2" runat="server" ControlToValidate="txtPassword" Display="Dynamic" ErrorMessage="Password is required" ForeColor="Red" text="*" ValidationGroup="oldcust"></asp:RequiredFieldValidator>
 
                </td>
            </tr>
     
            </table><br />
                     <asp:Button ID="cmdSubmit" runat="server" Text="Login" 
            onclick="cmdSubmit_Click" ValidationGroup="oldcust" />
               <br />
         <asp:Label ID="lblError1" runat="server"></asp:Label>
         <br />
               <asp:LinkButton ID="btnNewUser" runat="server" onclick="btnNewUser_Click">New User</asp:LinkButton>
             <asp:ValidationSummary ID="ValidationSummary1" runat="server" 
         ValidationGroup="oldcust" ForeColor="Red" HeaderText="The following errors were found:" />
               
        </asp:Panel>

 
        <asp:Panel ID="newuser_panel" runat="server" Height="386px" Width="411px">
              <table style="width: 401px">
                 <tr><td colspan="2">Setup your account here<br />All fields are required.</td></tr>
                  <tr><td colspan="2">
                      <table class="auto-style2">
                          <tr>
                              <td class="auto-style7">Email:</td>
                              <td class="auto-style8">
                                  <asp:TextBox ID="txtEmail" runat="server" ValidationGroup="newcust"></asp:TextBox>
                                  <asp:RequiredFieldValidator ID="rfvemail" runat="server" ErrorMessage="Required: Email" ControlToValidate="txtEmail" ForeColor="Red" ValidationGroup="newcust">*</asp:RequiredFieldValidator>
                                   </td>
                          </tr>
                          <tr>
                              <td class="auto-style4">Password:</td>
                              <td class="auto-style5">
                                  <asp:TextBox ID="txtPwd" runat="server" ValidationGroup="newcust"></asp:TextBox>
                                  <asp:RequiredFieldValidator ID="rfvpwd" runat="server" ErrorMessage="Required: Password" ControlToValidate="txtPwd" ForeColor="Red" ValidationGroup="newcust">*</asp:RequiredFieldValidator>
                              </td>
                          </tr>
                          <tr>
                              <td class="auto-style6">First Name:</td>
                              <td>
                                  <asp:TextBox ID="txtFName" runat="server" ValidationGroup="newcust"></asp:TextBox>
                              <asp:RequiredFieldValidator ID="rfvfname" runat="server" ErrorMessage="Required: First Name" ControlToValidate="txtFName" ForeColor="Red" ValidationGroup="newcust">*</asp:RequiredFieldValidator>
                              </td>
                          </tr>
                          <tr>
                              <td class="auto-style6">Last Name:</td>
                              <td>
                                  <asp:TextBox ID="txtLName" runat="server" ValidationGroup="newcust"></asp:TextBox>
                                  <asp:RequiredFieldValidator ID="rfvlname" runat="server" ErrorMessage="Required: Last Name" ControlToValidate="txtLName" ForeColor="Red" ValidationGroup="newcust">*</asp:RequiredFieldValidator>
                              </td>
                          </tr>
                          <tr>
                              <td class="auto-style6">Address:</td>
                              <td>
                                  <asp:TextBox ID="txtAddress" runat="server" ValidationGroup="newcust"></asp:TextBox>
                             <asp:RequiredFieldValidator ID="rfvadd" runat="server" ErrorMessage="Required: Address" ControlToValidate="txtAddress" ForeColor="Red" ValidationGroup="newcust">*</asp:RequiredFieldValidator>
                               </td>
                          </tr>
                          <tr>
                              <td class="auto-style6">City:</td>
                              <td>
                                  <asp:TextBox ID="txtCity" runat="server" ValidationGroup="newcust"></asp:TextBox>
                                  <asp:RequiredFieldValidator ID="rfvcity" runat="server" ErrorMessage="Required: City" ControlToValidate="txtCity" ForeColor="Red" ValidationGroup="newcust">*</asp:RequiredFieldValidator>
                              </td>
                          </tr>
                          <tr>
                              <td class="auto-style6">State:</td>
                              <td>
                                  <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource1" DataTextField="S_NAME" DataValueField="S_ABBREVIATION" ValidationGroup="newcust">
                                  </asp:DropDownList>
                              </td>
                          </tr>
                          <tr>
                              <td class="auto-style6">ZIP Code:</td>
                              <td>
                                  <asp:TextBox ID="txtZIP" runat="server" ValidationGroup="newcust"></asp:TextBox>
                                  <asp:RequiredFieldValidator ID="rfvzip" runat="server" ErrorMessage="Required: ZIP Code" ControlToValidate="txtZIP" ForeColor="Red" ValidationGroup="newcust">*</asp:RequiredFieldValidator>
                              </td>
                          </tr>
                          <tr>
                              <td class="auto-style6">Phone:</td>
                              <td>
                                  <asp:TextBox ID="txtPhone" runat="server" ValidationGroup="newcust"></asp:TextBox>
                                  <asp:RequiredFieldValidator ID="rfvphone" runat="server" ErrorMessage="Required: Phone Number" ControlToValidate="txtPhone" ForeColor="Red" ValidationGroup="newcust">*</asp:RequiredFieldValidator>
                              </td>
                          </tr>
                      </table>
                      </td></tr>
              </table>
            <br />
                <asp:Button ID="cmdAddUser" runat="server" Text="Add Me"  onclick="cmdAddUser_Click" ValidationGroup="newcust" />
              <br />
            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="newcust" ForeColor="Red" 
                        HeaderText="The following errors have been found:" />
            <br />
          <asp:LinkButton ID="btnExistingUser" runat="server" onclick="btnExistingUser_Click">Existing User</asp:LinkButton>
        <br />

         <asp:Label ID="lblError2" runat="server"></asp:Label>

               </asp:Panel>
   <br />


         <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:AcmeShoppeConnectionString %>" 
        ProviderName="<%$ ConnectionStrings:AcmeShoppeConnectionString.ProviderName %>" 
        SelectCommand="SELECT * FROM [States]">
              </asp:SqlDataSource>
    </div>
</asp:Content>

